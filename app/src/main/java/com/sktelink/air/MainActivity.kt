package com.sktelink.air

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Base64
import android.util.Log
import com.kakao.util.maps.helper.Utility.getPackageInfo
import com.sktelink.air.httputil.HttpAsyncTask
import com.sktelink.air.httputil.HttpString
import com.sktelink.air.ui.join.MemberJoinActivity
import com.sktelink.air.ui.main.MainFragment
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONException
import org.json.JSONObject
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

class MainActivity : BaseActivity() {

    lateinit var mIntent: Intent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init() {

        BaseActivity.APP_ENC_KEY = ""  // AesKey 초기화
        BaseActivity.Companion.sbParams = StringBuffer()
        BaseActivity.Companion.sbParams!!.append("version_info").append("=").append("0.0.1").append("&")
        BaseActivity.Companion.sbParams!!.append("os").append("=").append("A")

        HttpAsyncTask(
            this@MainActivity,
            HttpString.SAVANA_GET_CODE,
            BaseActivity.Companion.sbParams,
            mHandler,
            HttpString.REQUEST_METHOD_POST
        ).execute()
        //        String hashKey = getKeyHash(MainActivity.this);
        tv_btn_go_main.setOnClickListener {
            mIntent = Intent(this@MainActivity, MainFragment::class.java)
            startActivity(mIntent)
        }
        tv_btn_go_join.setOnClickListener {
            mIntent = Intent(this@MainActivity, MemberJoinActivity::class.java)
            startActivity(mIntent)
        }
    }

    companion object {

        fun getKeyHash(context: Context): String? {
            val packageInfo = getPackageInfo(context, PackageManager.GET_SIGNATURES) ?: return null

            for (signature in packageInfo!!.signatures) {
                try {
                    val md = MessageDigest.getInstance("SHA")
                    md.update(signature.toByteArray())
                    return Base64.encodeToString(md.digest(), Base64.NO_WRAP)
                } catch (e: NoSuchAlgorithmException) {
                    Log.w("catch", "Unable to get MessageDigest. signature=$signature", e)
                }

            }
            return null
        }
    }


    private val mHandler = object : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                BaseActivity.ERROR -> try {
                    BaseActivity.Companion.mJsonResponseData = JSONObject(msg.obj as String)

                } catch (e: Exception) {

                }

                BaseActivity.OK -> {
                    mJsonResponseData = msg.obj as JSONObject

                    val jsonObj = mJsonResponseData
                    try {
                        val encKey = jsonObj!!.getString("enc_key")
                        APP_ENC_KEY = encKey
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    mJsonResponseData = JSONObject()
                }
            }
        }
    }
}
