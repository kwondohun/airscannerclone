package com.sktelink.air.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.RelativeLayout

import com.sktelink.air.R

class HomeActivitySub2Adapter(private val mContext: Context) : BaseAdapter() {
    private val mLayoutInflater: LayoutInflater

    init {
        mLayoutInflater = LayoutInflater.from(mContext)
    }

    override fun getCount(): Int {
        return 30
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        val holder: ViewHolder
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.item_activity_home_sub_2, null)
            holder = ViewHolder()
            //            holder.mViewTopClear = (View) convertView.findViewById(R.id.view_top_clear);
            //            holder.mViewChild = (RelativeLayout) convertView.findViewById(R.id.view_child);
            //            holder.mIvRadioBtn = (ImageView) convertView.findViewById(R.id.iv_radio_btn);
            convertView!!.tag = holder
        } else {
            holder = convertView.tag as ViewHolder
        }


        //        holder.mIvRadioBtn.setTag(position);
        //        holder.mIvRadioBtn.setOnClickListener(new View.OnClickListener() {
        //            @Override
        //            public void onClick(View v) {
        ////                if ((Integer) holder.mIvRadioBtn.)
        //                if (AddressListActivity.mAddressChecked[position] == false)
        //                {
        //                    holder.mIvRadioBtn.setImageDrawable(mContext.getResources().getDrawable(R.drawable.icon_radio_on));
        //                    AddressListActivity.mAddressChecked[position] = true;
        //                }
        //                else
        //                {
        //                    holder.mIvRadioBtn.setImageDrawable(mContext.getResources().getDrawable(R.drawable.icon_radio_off));
        //                    AddressListActivity.mAddressChecked[position] = false;
        //                }
        //
        //
        //
        //                AddressListActivity.mAdapter.notifyDataSetChanged();
        //            }
        //        });
        return convertView
    }

    internal inner class ViewHolder//        View mViewTopClear;
    //        RelativeLayout mViewChild;
    //        ImageView mIvRadioBtn;
}
