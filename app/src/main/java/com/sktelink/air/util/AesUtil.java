package com.sktelink.air.util;

import android.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

public class AesUtil {
    public static final String SECRET_KEY = "b4b9c8f8594273eaa24a723205a01cc1"; //토큰값으로 대체될예상
    public static final String CHARSET_UTF_8 = "UTF-8";

    /**
     * Encryption mode enumeration
     */
    private enum EncryptMode {
        ENCRYPT, DECRYPT
    }

    // cipher to be used for encryption and decryption
    private Cipher _cx;

    // encryption key and initialization vector
    private byte[] _key, _iv;

    public AesUtil() throws NoSuchAlgorithmException, NoSuchPaddingException {
        // initialize the cipher with transformation AES/CBC/PKCS5Padding
        _cx = Cipher.getInstance("AES/CBC/PKCS5Padding");
        _key = new byte[32]; //256 bit key space
        _iv = new byte[16]; //128 bit IV
    }


    /**
     * @param inputText     Text to be encrypted or decrypted
     * @param encryptionKey Encryption key to used for encryption / decryption
     * @param mode          specify the mode encryption / decryption
     * @param initVector    Initialization vector
     * @return encrypted or decrypted bytes based on the mode
     * @throws UnsupportedEncodingException
     * @throws InvalidKeyException
     * @throws InvalidAlgorithmParameterException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    private byte[] encryptDecrypt(String inputText, String encryptionKey,
                                  EncryptMode mode, String initVector) throws UnsupportedEncodingException,
            InvalidKeyException, InvalidAlgorithmParameterException,
            IllegalBlockSizeException, BadPaddingException {

        SecretKeySpec keySpec = new SecretKeySpec(encryptionKey.getBytes(), "AES"); // Create a new SecretKeySpec for the specified key data and algorithm name.

        // encryption
        if (mode.equals(EncryptMode.ENCRYPT)) {
            _iv = initVector.getBytes();

            IvParameterSpec ivSpec = new IvParameterSpec(_iv); // Create a new IvParameterSpec instance with the bytes from the specified buffer iv used as initialization vector.

            _cx.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);// Initialize this cipher instance

            //Perform Encryption
            byte[] cipherText = _cx.doFinal(inputText.getBytes());
            byte[] encryptText = new byte[_iv.length + cipherText.length];
            System.arraycopy(_iv, 0, encryptText, 0, _iv.length);
            System.arraycopy(cipherText, 0, encryptText, _iv.length, cipherText.length);

            return encryptText;
        } else {
            byte[] value = Base64.decode(inputText, Base64.DEFAULT);
            byte[] ivVale = Arrays.copyOfRange(value, 0, 16);
            byte[] cipherVale = Arrays.copyOfRange(value, 16, value.length);

            _iv = ivVale;

            IvParameterSpec ivSpec = new IvParameterSpec(_iv); // Create a new IvParameterSpec instance with the bytes from the specified buffer iv used as initialization vector.

            _cx.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);// Initialize this cipher instance

            byte[] decryptedText = _cx.doFinal(cipherVale); // Finish multi-part transformation (decryption)

            return decryptedText;
        }
    }

    /***
     * This function computes the SHA256 hash of input string
     * @param text input text whose SHA256 hash has to be computed
     * @param length length of the text to be returned
     * @return returns SHA256 hash of input text
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     */
    private static String SHA256(String text, int length) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        return text;
    }


    public String encryptPlainText(String plainText, String key, String iv) throws Exception {
        byte[] bytes = encryptDecrypt(plainText, SHA256(key, 32), EncryptMode.ENCRYPT, iv);
        return Base64.encodeToString(bytes, Base64.DEFAULT);
    }

    public String decryptCipherText(String cipherText, String key, String iv) throws Exception {
        byte[] bytes = encryptDecrypt(cipherText, SHA256(key, 32), EncryptMode.DECRYPT, iv);
        return new String(bytes);
    }

    public String encryptPlainTextWithRandomIV(String plainText, String key) throws Exception {
        String iv = generateRandomIV16();
        byte[] bytes = encryptDecrypt(plainText, SHA256(key, 32), EncryptMode.ENCRYPT, iv);
//        return Base64.encodeToString(bytes, Base64.DEFAULT);
        String base64 = new String(
                Base64.encode(
                        bytes,
                        Base64.DEFAULT
                ),
                "UTF-8");
        return base64.trim();
    }

    public String encryptPlainTextWithRandomIV1(String plainText, String key) throws Exception {
        String iv = generateRandomIV16();
        byte[] bytes = encryptDecrypt(plainText, SHA256(key, 32), EncryptMode.ENCRYPT, iv);
        String base64 = new String(
                Base64.encode(
                        bytes,
                        Base64.DEFAULT
                ),
                "UTF-8");
        return base64.trim();
//        return Base64.encodeToString(bytes, Base64.DEFAULT|Base64.URL_SAFE|Base64.NO_WRAP|Base64.NO_PADDING);
    }

    public String decryptCipherTextWithRandomIV(String cipherText, String key) throws Exception {
        byte[] bytes = encryptDecrypt(cipherText, SHA256(key, 32), EncryptMode.DECRYPT, "");
        return new String(bytes);
    }


    /**
     * Generate IV with 16 bytes
     *
     * @return
     */
    public String generateRandomIV16() {
        SecureRandom ranGen = new SecureRandom();
        byte[] aesKey = new byte[16];
        ranGen.nextBytes(aesKey);
        StringBuilder result = new StringBuilder();
        for (byte b : aesKey) {
            result.append(String.format("%02x", b)); //convert to hex
        }
        if (16 > result.toString().length()) {
            return result.toString();
        } else {
            return result.toString().substring(0, 16);
        }
    }

    /**
     * Generate IV with 16 bytes
     *
     * @return
     */
    public String generateRandomIV16(String key) {
        return key.substring(0, 16);
    }
}
