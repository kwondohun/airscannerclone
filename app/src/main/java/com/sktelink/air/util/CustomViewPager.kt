package com.sktelink.air.util

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent

class CustomViewPager : ViewPager {
    private var mIsEnabled: Boolean = false

    constructor(context: Context) : super(context) {
        mIsEnabled = true
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        mIsEnabled = true
    }

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
        return if (mIsEnabled)
            super.onInterceptTouchEvent(ev)
        else
            false
    }
}