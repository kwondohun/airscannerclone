package com.sktelink.air.ui.popup

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import com.sktelink.air.R
import kotlinx.android.synthetic.main.popup_box_select.*

class PopupMobileChoice(context: Context?,
                        listenerSKT: View.OnClickListener?,
                        listenerKT: View.OnClickListener?,
                        listenerLGU: View.OnClickListener?) : Dialog(context) {

    private val mContext = context
    private val mClickListenerBtn4 = listenerSKT
    private val mClickListenerBtn6 = listenerKT
    private val mClickListenerBtn9 = listenerLGU

    override fun onCreate(savedInstanceState: Bundle?) {
        val lpWindow = WindowManager.LayoutParams()
        lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND
        lpWindow.dimAmount = 0.7f
        window!!.attributes = lpWindow
        setContentView(R.layout.popup_box_select)
        tv_btn_4.setOnClickListener(mClickListenerBtn4)
        tv_btn_6.setOnClickListener(mClickListenerBtn6)
        tv_btn_9.setOnClickListener(mClickListenerBtn9)
    }
}