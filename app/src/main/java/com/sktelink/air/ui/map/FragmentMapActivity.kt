package com.sktelink.air.ui.map

import android.Manifest
import android.content.Context.LOCATION_SERVICE
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import com.sktelink.air.R
import net.daum.mf.map.api.MapPoint
import net.daum.mf.map.api.MapReverseGeoCoder
import net.daum.mf.map.api.MapView

// 지도 프래그먼트
class FragmentMapActivity : Fragment(), MapView.CurrentLocationEventListener,
    MapReverseGeoCoder.ReverseGeoCodingResultListener, View.OnTouchListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    private var mTabLayout: TabLayout? = null
    private var mMapView: MapView? = null
    private var mWebView: WebView? = null
    private var mWebSetting: WebSettings? = null

    private var mLocationManager: LocationManager? = null
    private var mLongitude: Double = 0.toDouble()
    private var mLatitude: Double = 0.toDouble()

    fun EndFindLocation() {
        if (mLocationManager != null) {
            mLocationManager!!.removeUpdates(mLocationListener)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootGroup = inflater.inflate(R.layout.fragment_map, container, false) as ViewGroup
        mWebView = rootGroup.findViewById<View>(R.id.webview) as WebView
        mWebView!!.webChromeClient = WebChromeClient()
        mWebSetting = mWebView!!.settings
        mWebSetting!!.javaScriptEnabled = true

        mWebView!!.loadUrl("https://earth.nullschool.net/ko/#current/wind/surface/level/orthographic=-229.20,35.62,3000")

        mWebView!!.visibility = View.INVISIBLE
        mTabLayout = rootGroup.findViewById<View>(R.id.tab_layout) as TabLayout
        mTabLayout!!.addTab(mTabLayout!!.newTab().setText(resources.getString(R.string.korea_dust)))
        mTabLayout!!.addTab(mTabLayout!!.newTab().setText(resources.getString(R.string.world_dust)))
        mTabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                if (tab.position == 0) {
                    mWebView!!.visibility = View.GONE
                } else {
                    mWebView!!.visibility = View.VISIBLE
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

        mMapView = rootGroup.findViewById<View>(R.id.map_view) as MapView
        mLocationManager = activity!!.getSystemService(LOCATION_SERVICE) as LocationManager
        getMyLocation()

        mMapView!!.setCurrentLocationEventListener(this)
        return rootGroup
    }

    private fun getMyLocation() {
        val location = getLastKnownLocation()
        val provider = location!!.provider
        mLongitude = java.lang.Double.valueOf(String.format("%.6f", location.longitude))
        mLatitude = java.lang.Double.valueOf(String.format("%.6f", location.latitude))
        mMapView!!.setMapCenterPointAndZoomLevel(MapPoint.mapPointWithGeoCoord(mLatitude, mLongitude), 1, true)
    }

    private fun getLastKnownLocation(): Location? {
        mLocationManager = activity!!.applicationContext.getSystemService(LOCATION_SERVICE) as LocationManager
        val providers = mLocationManager!!.getProviders(true)
        var bestLocation: Location? = null
        for (provider in providers) {
            if (ActivityCompat.checkSelfPermission(
                    activity!!,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    activity!!, Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {

            }
            val l = mLocationManager!!.getLastKnownLocation(provider) ?: continue
            if (bestLocation == null || l.accuracy < bestLocation.accuracy) {
                // Found best last known location: %s", l);
                bestLocation = l
            }
        }
        return bestLocation
    }

    private val mLocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            //위치값이 갱신되면 이벤트가 발생.
            Log.d("linsoo", "onLocationChanged")
            EndFindLocation()

            val longitude = location.longitude //경도
            val latitude = location.latitude   //위도
            //            String address = getAddress(latitude, longitude);
            //            if(m_callback!=null)
            //                m_callback.callbackMethod(latitude,longitude, address );
        }

        override fun onStatusChanged(s: String, i: Int, bundle: Bundle) {

        }

        override fun onProviderEnabled(s: String) {

        }

        override fun onProviderDisabled(s: String) {

        }
    }

    override fun onReverseGeoCoderFoundAddress(mapReverseGeoCoder: MapReverseGeoCoder, s: String) {
        mapReverseGeoCoder.toString()
    }

    override fun onReverseGeoCoderFailedToFindAddress(mapReverseGeoCoder: MapReverseGeoCoder) {

    }

    override fun onCurrentLocationUpdate(mapView: MapView, currentLocation: MapPoint, v: Float) {
        val mapPointGeo = currentLocation.mapPointGeoCoord
    }

    override fun onCurrentLocationDeviceHeadingUpdate(mapView: MapView, v: Float) {

    }

    override fun onCurrentLocationUpdateFailed(mapView: MapView) {

    }

    override fun onCurrentLocationUpdateCancelled(mapView: MapView) {

    }

    override fun onTouch(view: View, motionEvent: MotionEvent): Boolean {
        val action = motionEvent.action
        when (action) {
            MotionEvent.ACTION_DOWN ->

                mMapView!!.parent.requestDisallowInterceptTouchEvent(true)

            MotionEvent.ACTION_UP ->

                mMapView!!.parent.requestDisallowInterceptTouchEvent(false)

            MotionEvent.ACTION_MOVE -> mMapView!!.parent.requestDisallowInterceptTouchEvent(true)
        }
        return mMapView!!.onTouchEvent(motionEvent)
    }
}