package com.sktelink.air.ui.home

import android.Manifest
import android.content.Context.LOCATION_SERVICE
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.v4.app.ActivityCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.TextView
import com.sktelink.air.BaseFragment
import com.sktelink.air.R
import com.sktelink.air.adapter.HomeActivitySub1Adapter
import com.sktelink.air.httputil.HttpAsyncTask
import com.sktelink.air.httputil.HttpString
import com.sktelink.air.util.GeoPoint
import com.sktelink.air.util.GeoTrans
import org.json.JSONException
import org.json.JSONObject

class FragmentHomeSub1 : BaseFragment() {

    private var mLocationManager: LocationManager? = null
    private var mGeoPoint: GeoPoint? = null
    private var tm_pt = GeoPoint(0.0, 0.0)
    private val mBCode = "" // 법정주소코드
    private val mHCode = "" // 행정주소코드
    private val mBAddr = "" // 법정주소
    private val mHAddr = "" // 행정주소

    private var mTvMyAddr: TextView? = null

    private var mHomeActivitySub1Adapter: HomeActivitySub1Adapter? = null
    private var mHomeSub1ListView: ListView? = null


    private// Found best last known location: %s", l);
    val lastKnownLocation: Location?
        get() {
            mLocationManager = activity!!.applicationContext.getSystemService(LOCATION_SERVICE) as LocationManager
            val providers = mLocationManager!!.getProviders(true)
            var bestLocation: Location? = null
            for (provider in providers) {
                if (ActivityCompat.checkSelfPermission(
                        activity!!,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        activity!!, Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {

                }
                val l = mLocationManager!!.getLastKnownLocation(provider) ?: continue
                if (bestLocation == null || l.accuracy < bestLocation.accuracy) {
                    bestLocation = l
                }
            }
            return bestLocation
        }

    private val mResponseHandler = object : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                ERROR -> try {
                    mJsonResponseData = JSONObject(msg.obj as String)
                } catch (e: Exception) {

                }

                OK -> mJsonResponseData = msg.obj as JSONObject
            }//                    try {
            //                        JSONArray jsonArr = mJsonResponseData.getJSONArray("documents");  // 에러메시지가 있을경우
            //                        JSONObject jsonMeta = mJsonResponseData.getJSONObject("meta");
            //                        int totalCnt = (int)(jsonMeta.get("total_count"));
            //
            //                        JSONObject BData = new JSONObject();
            //                        JSONObject HData = new JSONObject();
            //                        for(int i = 0 ; i < totalCnt ; i++)
            //                        {
            //                            String regionType = ((JSONObject) jsonArr.get(i)).getString("region_type");
            //                            if(regionType.equals("B"))
            //                            {
            //                                BData = (JSONObject) jsonArr.get(i); // 법정동 데이터
            //                            }
            //                            else if(regionType.equals("H"))
            //                            {
            //                                HData = (JSONObject) jsonArr.get(i); // 행정동 데이터
            //                            }
            //                        }
            //                        mBCode = BData.getString("code");  // 행정구역코드
            //                        mHCode = HData.getString("code");  // 법정구역코드
            //
            //                        mHAddr = HData.getString("address_name");
            //                        mBAddr = BData.getString("address_name");
            //                        mJsonResponseData = new JSONObject();
            //
            //                        getActivity().runOnUiThread(new Runnable() {
            //                            @Override
            //                            public void run() {
            //                                mTvMyAddr.setText(mHAddr);
            //                            }
            //                        });
            //                    } catch (JSONException e) {
            //                        e.printStackTrace();
            //                    }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootGroup = inflater.inflate(R.layout.fragment_home_sub_1, container, false) as ViewGroup
        mTvMyAddr = rootGroup.findViewById<View>(R.id.tv_my_addr) as TextView
        val location = lastKnownLocation
        val longitude = location!!.longitude
        val latitude = location.latitude

        mGeoPoint = GeoPoint()
        mGeoPoint!!.x = longitude
        mGeoPoint!!.y = latitude
        tm_pt = GeoTrans.convert(GeoTrans.GEO, GeoTrans.TM, mGeoPoint)

        mSbParams = StringBuffer()
        mSbParams!!.append("?").append("x").append("=").append(longitude).append("&") // 위도
        mSbParams!!.append("y").append("=").append(latitude) //경도
        HttpAsyncTask(
            activity,
            HttpString.KAKAO_API,
            mSbParams,
            mResponseHandler,
            HttpString.REQUEST_METHOD_GET,
            HttpString.KAKAO_API_KEY
        ).execute()

        mHomeActivitySub1Adapter = HomeActivitySub1Adapter(activity!!)
        mHomeSub1ListView = rootGroup.findViewById<View>(R.id.listview) as ListView
        mHomeSub1ListView!!.adapter = mHomeActivitySub1Adapter
        return rootGroup
    }
}