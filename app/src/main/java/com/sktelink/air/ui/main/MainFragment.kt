package com.sktelink.air.ui.main

import android.Manifest
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.View
import android.view.Window
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.widget.TextView
import com.sktelink.air.BaseActivity
import com.sktelink.air.R
import com.sktelink.air.util.CustomViewPager
import com.sktelink.air.ui.home.FragmentHomeSub1
import com.sktelink.air.ui.home.FragmentHomeSub2
import net.daum.mf.map.api.MapPoint
import net.daum.mf.map.api.MapReverseGeoCoder
import net.daum.mf.map.api.MapView

class MainFragment : BaseActivity(), MapView.CurrentLocationEventListener,
    MapReverseGeoCoder.ReverseGeoCodingResultListener {

    private var mPager: CustomViewPager? = null
    private var mTvBtn_1: TextView? = null
    private var mTvBtn_2: TextView? = null
    private var mTvBtn_3: TextView? = null
    private var mTvBtn_4: TextView? = null
    private var mTvBtn_5: TextView? = null

    private var mViewHome: View? = null
    private var mViewMap: View? = null
    private var mViewInfo: View? = null
    private var mViewMyPage: View? = null


    // 홈


    // 맵
    private var mTabLayout: TabLayout? = null
    private var mMapView: MapView? = null
    private var mWebView: WebView? = null
    private var mWebSetting: WebSettings? = null
    private var mLocationManager: LocationManager? = null
    private var mLongitude: Double = 0.toDouble()
    private var mLatitude: Double = 0.toDouble()

    internal var movePageListener: View.OnClickListener = View.OnClickListener { v ->
        //            int tag = (int) v.getTag();
        //            mPager.setCurrentItem(tag);
        mAllViewInvisible()
        when (v.id) {
            R.id.tv_tab_1 -> mViewHome!!.visibility = View.VISIBLE

            R.id.tv_tab_2 -> mViewMap!!.visibility = View.VISIBLE

            R.id.tv_tab_3 -> {
            }

            R.id.tv_tab_4 ->

                mViewInfo!!.visibility = View.VISIBLE

            R.id.tv_tab_5 -> mViewMyPage!!.visibility = View.VISIBLE

            else -> {
            }
        }
    }

    private val mLocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            //위치값이 갱신되면 이벤트가 발생.
            Log.d("linsoo", "onLocationChanged")
            EndFindLocation()

            val longitude = location.longitude //경도
            val latitude = location.latitude   //위도
            //            String address = getAddress(latitude, longitude);
            //            if(m_callback!=null)
            //                m_callback.callbackMethod(latitude,longitude, address );
        }

        override fun onStatusChanged(s: String, i: Int, bundle: Bundle) {

        }

        override fun onProviderEnabled(s: String) {

        }

        override fun onProviderDisabled(s: String) {

        }
    }

    private// Found best last known location: %s", l);
    val lastKnownLocation: Location?
        get() {
            mLocationManager = applicationContext.getSystemService(LOCATION_SERVICE) as LocationManager
            val providers = mLocationManager!!.getProviders(true)
            var bestLocation: Location? = null
            for (provider in providers) {
                if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {

                }
                val l = mLocationManager!!.getLastKnownLocation(provider) ?: continue
                if (bestLocation == null || l.accuracy < bestLocation.accuracy) {
                    bestLocation = l
                }
            }
            return bestLocation
        }


    override fun onCreate(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {

        }
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_main)

        init()
    }

    //    @Override
    //    public boolean dispatchTouchEvent(MotionEvent ev) {
    //        mPager.requestDisallowInterceptTouchEvent(true);
    //        return super.dispatchTouchEvent(ev);
    //    }

    private fun init() {
        if (Build.VERSION.SDK_INT >= 21) {
            // 21 버전 이상일 때만 상태바 컨트롤 가능
            window.statusBarColor = resources.getColor(R.color.cerulean)
        }
        mPager = findViewById<View>(R.id.pager) as CustomViewPager
        mPager!!.setAdapter(mPagerAdapter(supportFragmentManager))

        //        mPager.setOnTouchListener(new View.OnTouchListener() {
        //            @Override
        //            public boolean onTouch(View view, MotionEvent motionEvent) {
        //                return true;
        //            }
        //        });
        mPager!!.setCurrentItem(0)

        mViewHome = findViewById(R.id.view_home) as View
        mViewMap = findViewById(R.id.view_map) as View
        mViewInfo = findViewById(R.id.view_info) as View
        mViewMyPage = findViewById(R.id.view_mypage) as View



        mTvBtn_1 = findViewById<View>(R.id.tv_tab_1) as TextView
        mTvBtn_2 = findViewById<View>(R.id.tv_tab_2) as TextView
        mTvBtn_3 = findViewById<View>(R.id.tv_tab_3) as TextView
        mTvBtn_4 = findViewById<View>(R.id.tv_tab_4) as TextView
        mTvBtn_5 = findViewById<View>(R.id.tv_tab_5) as TextView

        mTvBtn_1!!.setOnClickListener(movePageListener)
        mTvBtn_2!!.setOnClickListener(movePageListener)
        mTvBtn_3!!.setOnClickListener(movePageListener)
        mTvBtn_4!!.setOnClickListener(movePageListener)
        mTvBtn_5!!.setOnClickListener(movePageListener)

        mTvBtn_1!!.tag = 0
        mTvBtn_2!!.tag = 1
        mTvBtn_3!!.tag = 2
        mTvBtn_4!!.tag = 3
        mTvBtn_5!!.tag = 4

        mPager!!.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(i: Int, v: Float, i1: Int) {

            }

            override fun onPageSelected(position: Int) {
                mPager!!.setCurrentItem(position)

            }

            override fun onPageScrollStateChanged(i: Int) {

            }
        })

        // 홈


        // 맵
        mWebView = findViewById<View>(R.id.webview) as WebView
        mWebView!!.webChromeClient = WebChromeClient()
        mWebSetting = mWebView!!.settings
        mWebSetting!!.javaScriptEnabled = true

        mWebView!!.loadUrl("https://earth.nullschool.net/ko/#current/wind/surface/level/orthographic=-229.20,35.62,3000")

        mWebView!!.visibility = View.INVISIBLE
        mTabLayout = findViewById<View>(R.id.tab_layout) as TabLayout
        mTabLayout!!.addTab(mTabLayout!!.newTab().setText(resources.getString(R.string.korea_dust)))
        mTabLayout!!.addTab(mTabLayout!!.newTab().setText(resources.getString(R.string.world_dust)))
        mTabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                if (tab.position == 0) {
                    mWebView!!.visibility = View.GONE
                } else {
                    mWebView!!.visibility = View.VISIBLE
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

        mMapView = findViewById<View>(R.id.map_view) as MapView
        mLocationManager = getSystemService(LOCATION_SERVICE) as LocationManager
        getMyLocation()

        mMapView!!.setCurrentLocationEventListener(this)
    }

    private fun mAllViewInvisible() {
        mViewHome!!.visibility = View.INVISIBLE
        mViewMap!!.visibility = View.INVISIBLE
        mViewInfo!!.visibility = View.INVISIBLE
        mViewMyPage!!.visibility = View.INVISIBLE
    }


    ///// 맵
    fun EndFindLocation() {
        if (mLocationManager != null) {
            mLocationManager!!.removeUpdates(mLocationListener)
        }
    }

    override fun onReverseGeoCoderFoundAddress(mapReverseGeoCoder: MapReverseGeoCoder, s: String) {

    }

    override fun onReverseGeoCoderFailedToFindAddress(mapReverseGeoCoder: MapReverseGeoCoder) {

    }

    override fun onCurrentLocationUpdate(mapView: MapView, mapPoint: MapPoint, v: Float) {

    }

    override fun onCurrentLocationDeviceHeadingUpdate(mapView: MapView, v: Float) {

    }

    override fun onCurrentLocationUpdateFailed(mapView: MapView) {

    }

    override fun onCurrentLocationUpdateCancelled(mapView: MapView) {

    }

    private fun getMyLocation() {
        val location = lastKnownLocation
        val provider = location!!.provider
        mLongitude = java.lang.Double.valueOf(String.format("%.6f", location.longitude))
        mLatitude = java.lang.Double.valueOf(String.format("%.6f", location.latitude))
        mMapView!!.setMapCenterPointAndZoomLevel(MapPoint.mapPointWithGeoCoord(mLatitude, mLongitude), 1, true)
    }


    // 메인 컨트롤
    private inner class mPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

        override fun getItem(position: Int): Fragment? {
            when (position) {
                0 ->

                    return FragmentHomeSub1()
                1 ->

                    return FragmentHomeSub2()
                2 -> return null
                //                    return new SnapShotActivity();
                //                case 3:
                //                    return new FragmentInfoActivity();
                //                case 4:
                //                    return new FragmentMyPageActivity();
                else -> return null
            }
        }

        override fun getCount(): Int {
            // total page count
            return 2
        }
    }
}