package com.sktelink.air.ui.home

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.sktelink.air.R

// 홈 프래그먼트
class FragmentHomeActivity : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    private var mPager: ViewPager? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootGroup = inflater.inflate(R.layout.fragment_home, container, false) as ViewGroup
        mPager = rootGroup.findViewById<View>(R.id.pager) as ViewPager
        mPager!!.adapter = mPagerAdapter(activity!!.supportFragmentManager)
        mPager!!.currentItem = 0
        var fg: Fragment
        fg = FragmentHomeSub1()
        setChildFragment(fg)
        mPager!!.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(i: Int, v: Float, i1: Int) {

            }
            override fun onPageSelected(position: Int) {
                if (position == 0) {
                    fg = FragmentHomeSub1()
                } else {
                    fg = FragmentHomeSub2()
                }
                setChildFragment(fg)
                mPager!!.currentItem = position
            }

            override fun onPageScrollStateChanged(i: Int) {
                Log.d("", i.toString() + "")
            }
        })


        return rootGroup
    }

    private fun setChildFragment(child: Fragment) {
        val childFt = childFragmentManager.beginTransaction()

        if (!child.isAdded) {
            childFt.replace(R.id.child_fragment_container, child)
            childFt.addToBackStack(null)
            childFt.commit()
        }
    }

    private inner class mPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

        override fun getItem(position: Int): Fragment? {
            when (position) {
                0 -> return FragmentHomeSub1()
                1 -> return FragmentHomeSub2()
                else -> return null
            }
        }

        override fun getCount(): Int {
            // total page count
            return 2
        }
    }
}