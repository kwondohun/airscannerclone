package com.sktelink.air.ui.join

import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.TextView
import com.sktelink.air.BaseActivity
import com.sktelink.air.R
import com.sktelink.air.httputil.HttpAsyncTask
import com.sktelink.air.httputil.HttpString
import com.sktelink.air.ui.popup.PopupMobileChoice
import kotlinx.android.synthetic.main.activity_member_join.*
import org.json.JSONException
import org.json.JSONObject

class MemberJoinActivity: BaseActivity() {

    private var mEtName: EditText? = null
    private var mRelativeMan: RelativeLayout? = null
    private var mRelativeWoman: RelativeLayout? = null
    private var mEtUserNum: EditText? = null
    private var mTvMobile: TextView? = null
    private var mEtPhoneNum: EditText? = null
    private var mBtnAuthRequest: Button? = null
    private var mEtOtpNum: EditText? = null
    private var mBtnAuthOK: Button? = null
    private var mBtnJoin: Button? = null

    private var mMobileCode = 0

    private var mPopupMobileChoice: PopupMobileChoice? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_member_join)

        init()
    }

    private fun init() {
        mEtName = findViewById<View>(R.id.et_name) as EditText
        mRelativeMan = findViewById<View>(R.id.relative_man) as RelativeLayout
        mRelativeWoman = findViewById<View>(R.id.relative_woman) as RelativeLayout
        mEtUserNum = findViewById<View>(R.id.et_birthday) as EditText
        mTvMobile = findViewById<View>(R.id.tv_mobile) as TextView
        mEtPhoneNum = findViewById<View>(R.id.et_phone) as EditText
        mBtnAuthRequest = findViewById<View>(R.id.btn_auth_request) as Button
        mEtOtpNum = findViewById<View>(R.id.et_otp_num) as EditText
        mBtnAuthOK = findViewById<View>(R.id.btn_auth_ok) as Button
        mBtnJoin = findViewById<View>(R.id.btn_join) as Button

        tv_mobile.setOnClickListener(mOnClickListener)
        relative_man.setOnClickListener(mOnClickListener)
        relative_woman.setOnClickListener(mOnClickListener)
        btn_auth_request.setOnClickListener(mOnClickListener)
        btn_auth_ok.setOnClickListener(mOnClickListener)
    }

    private val mHandler = object : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                BaseActivity.ERROR -> try {
                    BaseActivity.mJsonResponseData = JSONObject(msg.obj as String)
                } catch (e: Exception) {
                }

                BaseActivity.OK -> {
                    BaseActivity.mJsonResponseData = msg.obj as JSONObject
                    try {

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    BaseActivity.mJsonResponseData = JSONObject()
                }
            }
        }
    }

    private val mOnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.relative_man -> {
            }

            R.id.relative_woman -> {
            }

            R.id.tv_mobile -> {
                mPopupMobileChoice = PopupMobileChoice(
                    this@MemberJoinActivity,
                    mOnClickListenerSKT,
                    mOnClickListenerKT,
                    mOnClickListenerLGU
                )
                mPopupMobileChoice?.show()
            }

            R.id.btn_auth_request -> {
                BaseActivity.Companion.sbParams = StringBuffer()
                sbParams?.append("cell_phn_no")?.append("=")?.append(mEtPhoneNum!!.getText().toString())?.append("&")
                sbParams?.append("birth_s")?.append("=")?.append(mEtUserNum!!.getText().toString() + "1")?.append("&")
                sbParams?.append("cust_nm")?.append("=")?.append(mEtName!!.getText().toString())?.append("&")
                sbParams?.append("mb_code")?.append("=")?.append(mMobileCode.toString())
                HttpAsyncTask(
                    this@MemberJoinActivity,
                    HttpString.SAVANA_GET_AUTH,
                    sbParams!!,
                    mHandler,
                    HttpString.REQUEST_METHOD_POST
                ).execute()
            }

            R.id.btn_auth_ok -> {
            }

            R.id.btn_join -> {
            }

            else -> {
            }
        }
    }

    private val mOnClickListenerSKT = View.OnClickListener {
        mTvMobile!!.setText("SKT")
        mMobileCode = 1
        mPopupMobileChoice!!.dismiss()
    }

    private val mOnClickListenerKT = View.OnClickListener {
        mTvMobile!!.setText("KT")
        mMobileCode = 2
        mPopupMobileChoice!!.dismiss()
    }

    private val mOnClickListenerLGU = View.OnClickListener {
        mTvMobile!!.setText("LGU+")
        mMobileCode = 3
        mPopupMobileChoice!!.dismiss()
    }
}