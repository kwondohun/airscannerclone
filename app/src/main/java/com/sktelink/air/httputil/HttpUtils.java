package com.sktelink.air.httputil;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.util.zip.GZIPInputStream;

public class HttpUtils {
    public static final String DEFAULT_CHARSET       = "utf-8";
    public static final String METHOD_POST           = "POST";
    public static final String METHOD_GET            = "GET";
    public static final String CONTENT_ENCODING_GZIP = "gzip";

    public static HttpResponseEntity getResponseAsResponseEntity(HttpURLConnection conn) throws IOException {
        HttpResponseEntity responseEntity = new HttpResponseEntity();
        String charset = getResponseCharset(conn.getContentType());
        InputStream es = conn.getErrorStream();

        responseEntity.setStatusCode(conn.getResponseCode());
        if (es == null) {
            String contentEncoding = conn.getContentEncoding();
            if (CONTENT_ENCODING_GZIP.equalsIgnoreCase(contentEncoding)) {
                responseEntity.setBody(getStreamAsString(new GZIPInputStream(conn.getInputStream()), charset));
            } else {
                responseEntity.setBody(getStreamAsString(conn.getInputStream(), charset));
            }
        } else {
            String msg = getStreamAsString(es, charset);
            if (StringUtils.isEmpty(msg)) {
                responseEntity.setBody(conn.getResponseCode() + ":" + conn.getResponseMessage());
            } else {
                responseEntity.setBody(msg);
            }
        }

        return responseEntity;
    }

    private static String getResponseCharset(String ctype) {
        String charset = DEFAULT_CHARSET;

        if (!StringUtils.isEmpty(ctype)) {
            String[] params = ctype.split(";");
            for (String param : params) {
                param = param.trim();
                if (param.startsWith("charset")) {
                    String[] pair = param.split("=", 2);
                    if (pair.length == 2) {
                        if (!StringUtils.isEmpty(pair[1])) {
                            charset = pair[1].trim();
                        }
                    }
                    break;
                }
            }
        }

        return charset;
    }

    private static String getStreamAsString(InputStream stream, String charset) throws IOException {
        try {
            Reader reader = new InputStreamReader(stream, charset);
            StringBuilder response = new StringBuilder();

            final char[] buff = new char[1024];
            int read = 0;
            while ((read = reader.read(buff)) > 0) {
                response.append(buff, 0, read);
            }

            return response.toString();
        } finally {
            if (stream != null) {
                stream.close();
            }
        }
    }
}
