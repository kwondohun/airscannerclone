package com.sktelink.air.httputil;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.sktelink.air.BaseActivity;
import com.sktelink.air.util.AesUtil;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class HttpAsyncTask extends AsyncTask<String, String, String> {

    public static String mEncKey = "";
    private Handler mHandler;
    private Exception mException;
    private String mApiStr;
    private StringBuffer mSbf;
    private JSONObject mJoinJsonResponse;
    private Context mContext;
    private String mRequestMethod = "";
    private String mStrAuthorization = "";

    String mMessage = "";
    public static HttpURLConnection http = null;
    /**
     *
     * @param context
     * @param api
     * @param sbf
     * @param hanlder
     * @param requestMethod
     */
    public HttpAsyncTask(Context context, String api, StringBuffer sbf, Handler hanlder, String requestMethod){
        this.mContext = context;
        this.mApiStr = api;
        this.mSbf = sbf;
        this.mHandler = hanlder;
        this.mRequestMethod = requestMethod;
    }
    /**
     *
     * @param context
     * @param api
     * @param sbf
     * @param hanlder
     * @param requestMethod
     * @param strAuthorization
     */
    public HttpAsyncTask(Context context, String api, StringBuffer sbf, Handler hanlder, String requestMethod, String strAuthorization){
        this.mContext = context;
        this.mApiStr = api;
        this.mSbf = sbf;
        this.mHandler = hanlder;
        this.mRequestMethod = requestMethod;
        this.mStrAuthorization = strAuthorization;
    }

    @Override
    protected void onPostExecute(String string) {
        if(mException != null)
        {
            handlerSetting(ERROR);

            return;
        }
        else
        {
            if(!mMessage.equals("")) // 통신에 성공했을 경우
            {
                try {
                    mJoinJsonResponse = new JSONObject(mMessage);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            handlerSetting(OK);
        }
    }

    @Override
    protected String doInBackground(String... strings) {
        if(BaseActivity.Companion.getAPP_ENC_KEY().equals(""))
        {
            mEncKey = AesUtil.SECRET_KEY;
        }
        else
        {
            mEncKey = BaseActivity.Companion.getAPP_ENC_KEY();
        }
        try
        {
            URL url;
            if(mRequestMethod.equals(HttpString.Companion.getREQUEST_METHOD_POST()))
            {
                String encryptBody = BaseActivity.Companion.encrypt1(BaseActivity.Companion.getSbParams().toString(), mEncKey);

                url = new URL(mApiStr);
                http = (HttpURLConnection) url.openConnection();
                http.setDoOutput(true);
                http.setDoInput(true);
                http.setDefaultUseCaches(false);
                http.setRequestMethod(mRequestMethod);
                http.setRequestProperty("Content-Type","application/x-www-form-urlencoded");

                // request
                OutputStream os = null;
                os = http.getOutputStream();
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                bw.write(encryptBody);
                bw.flush();
                bw.close();
                os.close();
            }
            else if(mRequestMethod.equals(HttpString.Companion.getREQUEST_METHOD_GET()))
            {
                url = new URL(mApiStr + mSbf);
                http = (HttpURLConnection) url.openConnection();
                http.setDoInput(true);
                http.setDefaultUseCaches(false);
                http.setRequestMethod(mRequestMethod);
                http.setChunkedStreamingMode(0);

                if(!mStrAuthorization.equals(""))   // Authorization 유
                {
                    http.setRequestProperty("Authorization", mStrAuthorization);
                }
            }
            else if(mRequestMethod.equals(HttpString.Companion.getREQUEST_DUST_DATA()))
            {
                url = new URL(mApiStr.toString() + mSbf);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-type", "application/json");
                System.out.println("Response code: " + conn.getResponseCode());
                BufferedReader rd;
                if(conn.getResponseCode() >= 200 && conn.getResponseCode() <= 300) {
                    rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                } else {
                    rd = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
                }
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = rd.readLine()) != null) {
                    sb.append(line);
                }
                rd.close();
                conn.disconnect();

                String dataStr;
                JSONObject jsonObj;
                try {
                    jsonObj = XML.toJSONObject(sb.toString());

                    JSONObject jsondata2 = jsonObj;

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else if(mRequestMethod.equals(HttpString.Companion.getREQUEST_MEASUREMENT_DATA()))
            {
                url = new URL(mApiStr.toString() + mSbf);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-type", "application/json");
                System.out.println("Response code: " + conn.getResponseCode());
                BufferedReader rd;
                if(conn.getResponseCode() >= 200 && conn.getResponseCode() <= 300) {
                    rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                } else {
                    rd = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
                }
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = rd.readLine()) != null) {
                    sb.append(line);
                }
                rd.close();
                conn.disconnect();

                String dataStr;
                JSONObject jsonObj;
                try {
                    jsonObj = XML.toJSONObject(sb.toString());

                    JSONObject jsondata2 = jsonObj;

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            Log.e("getResponseCode() = " , http.getResponseCode()+"");
            // response
            if(http.getResponseCode() == http.HTTP_OK)
            {
                InputStreamReader tmp = new InputStreamReader(http.getInputStream(), "UTF-8");
                BufferedReader reader = new BufferedReader(tmp);
                StringBuilder builder = new StringBuilder();
                String str;
                while ((str = reader.readLine()) != null)
                {
                    builder.append(str + "\n");
                }

                try {
                    String newResponseBodyText = BaseActivity.Companion.decrypt(builder.toString(), AesUtil.SECRET_KEY);
                    mJoinJsonResponse = new JSONObject(newResponseBodyText);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else
            {
                HttpResponseEntity responseEntity = new HttpResponseEntity();
                responseEntity = HttpUtils.getResponseAsResponseEntity(http);
                mMessage = responseEntity.getBody();
            }
        }
        catch (MalformedURLException e){

        }
        catch (IOException e) {
            mException = e;
            return null;
        }
        return "ok";
    }

    String[] temp = new String[3];
    String[] wfEn = new String[3];
    String[] hour1 = new String[3];

    String m_cookies = "";
    boolean m_session = false;
    public void saveCookie( HttpURLConnection conn)
    {
        Map<String, List<String>> imap = conn.getHeaderFields( ) ;
        if( imap.containsKey( "Set-Cookie" ) )
        {
            List<String> lString = imap.get( "Set-Cookie" ) ;
            for( int i = 0 ; i < lString.size() ; i++ ) {
                m_cookies += lString.get( i ) ;
            }
            Log.e("zdg",m_cookies);
            m_session = true ;
        } else {
            m_session = false ;
        }
    }
    private final int ERROR = -1;
    private final int OK = 0;
    @Override
    protected void onPreExecute() {

    }

    private void handlerSetting(int what)
    {
        Message msg = mHandler.obtainMessage();
        msg.what = what;
        msg.obj = mJoinJsonResponse;
        mHandler.sendMessage(msg);
    }
}
