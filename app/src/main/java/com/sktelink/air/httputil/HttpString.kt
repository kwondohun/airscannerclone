package com.sktelink.air.httputil

class HttpString {
    companion object{
        var REQUEST_METHOD_POST = "POST"
        var REQUEST_METHOD_GET = "GET"
        var REQUEST_METHOD_PATCH = "PATCH"
        var REQUEST_METHOD_PUT = "PUT"
        var REQUEST_METHOD_DELETE = "DELETE"

        var REQUEST_WEATHER_DATA = "WEATHER"
        var REQUEST_DUST_DATA = "DUST"
        var REQUEST_MEASUREMENT_DATA = "MEASUREMENT"

        /* API LIST */
        // KEY는 측정소리스트, 미세먼지정보 공용으로 사용
        val AIR_KOREA_DUST_API_KEY =
            "H9N2MOzJCNGZhgiJ%2BVtLu%2BmlA96PHuv812XqeWtzIMfyX6iZd4YiT3mWIf8EU2fkqVhohHWNzRNnnIgmfJMNAQ%3D%3D"

        // 미세먼지 URL
        val AIR_KOREA_DUST_BASE_URL = "http://openapi.airkorea.or.kr/openapi/services/rest/ArpltnInforInqireSvc"  //
        val DUST_SUB_URL_1 = "/getCtprvnRltmMesureDnsty"  // 시도별
        val DUST_SUB_URL_2 = "/getMsrstnAcctoRltmMesureDnsty"  // 측정소별
        val DUST_SUB_URL_3 = "/getMinuDustFrcstDspth"  // 미세먼지예보

        // 측정소 URL
        val AIR_KOREA_MEASUREMENT_BASE_URL = "http://openapi.airkorea.or.kr/openapi/services/rest/MsrstnInfoInqireSvc"
        val MEASUREMENT_SUB_URL_1 = "/getNearbyMsrstnList"  // 측정소 리스트

        // 카카오 API
        val KAKAO_API_KEY = "KakaoAK 7ccbbb10b70aac713de63cc21e91272b"
        val KAKAO_API = "https://dapi.kakao.com/v2/local/geo/coord2regioncode.json"

        // 기상청 API
        val WEATHER_API = "http://www.kma.go.kr/wid/queryDFSRSS.jsp"

        // BASE_URL
        private val SAVANA_BASE_URL = "https://stg.savanago.com/mobileapp"
        // 최초 암호화 키 받아오기
        val SAVANA_GET_CODE = "$SAVANA_BASE_URL/common/v100/system/get-code"
        // SMS 인증
        val SAVANA_GET_AUTH = "$SAVANA_BASE_URL/common/v100/system/req-niceauthotp"
    }
}