package com.sktelink.air

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.json.JSONObject

open class BaseFragment() : Fragment() {
    protected var mJsonResponseData: JSONObject? = null
    protected var mSbParams: StringBuffer? = null
    protected val ERROR = -1
    protected val OK = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return super.onCreateView(inflater, container, savedInstanceState)
    }
}